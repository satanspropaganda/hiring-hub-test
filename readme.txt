Hey Hiring Hub!

So this is what I have built, the brief was a little loose for what is best to do frontend wise. It felt a little backend heavy still, but I thought if I used frontend technologies for it it'd demonstrate my skills across the frontend spectrum. The SCSS was compiled locally, I have supplied my source files for you to look through alongside the compiled result
My primary technologies normally are HTML, SCSS and JQuery, however I thought it'd be interesting to use Regex and standard JS to validate the number

It was a bit of a challenge validation wise, but the frontend I kept simple. Links to some of my actual recent frontend work are below, I just didn't want to overcomplicate something that seemed like it was mainly about the functionality

https://anolon.co.uk/
https://www.pushon.co.uk/
https://invopak.co.uk/

If this isn't enough for the test, please say! I am more than happy to add more to it,

Thanks,
Benny