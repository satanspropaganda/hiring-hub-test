function validate() {
  var num = document.getElementsByClassName("card-number")[0].value;
  var brand = null;
  if(/^[3][4|7].*$/.test(num)){
    brand = "amex";
  } else if (/^[6011].*$/.test(num)) {
    brand = "discovery";
  } else if (/^[51|55].*$/.test(num)) {
    brand = "mastercard";
  } else if (/^[4].*$/.test(num)) {
    brand = "visa";
  }
  document.getElementById("logo").className = brand;
  var numbers = num.replace(/\s+/g, "");
  numbers = numbers.split("");
  numbers = numbers.reverse();
  numbers = numbers.map(function(v) {
    return parseInt(v, 10);
  });
  var multiply = [];
  var normal = [];
  for (var i = 0; i < numbers.length; i++) {
    if(i % 2 === 0) { // index is even
      normal.push(numbers[i]);
    } else {
    var sum = numbers[i] * 2;
      multiply.push(sum);
    }
  }
  var sum = 0;
  for (var i = 0; i < multiply.length; i++) {
    var value = multiply[i];
    sum += value % 10;
    value = Math.floor(value / 10);
    sum = sum + value;
  }
  for (var i = 0; i < normal.length; i++) {
    sum = sum + normal[i];
  }
  if (num.length >= 13 && brand != null && sum % 10 === 0) {
    document.getElementById("validity").innerHTML = "Valid Debit";
  } else {
    document.getElementById("validity").innerHTML = "Not Valid Debit";
  }
}